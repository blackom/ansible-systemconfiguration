# Ansible - Complete system with OpenVPN function

Ansible roles and playbook to install complete system with specific functions

This repository includes pices from repository: https://github.com/BastiPaeltz/ansible-openvpn

## Description:
This playbook has functions that can be used for build full GUI
system, OpenVPN server, clear system with pentesting software or whatsoever.

## Functions:
- Checking ssh port
- Configuration apt with debian testing repository
- Create new user which is used in next step of configuration
- Configuration ssh config file
- Installing and configuration docker
- Configuration iptables with simple rule
- Checkout and pull repository from git
- Configuration OpenVPN server and clients
- Installing VirtualBox extensions
- Installing x11

You can find configuration files in inventories/sample.


## How to start:
First of all you should rename sample folder in inventories for yours project name. If you do
that you should configure variables in groups_vars/all.yml. You will find full
description of each variables in this file. Next step is to configure
host_vars where you create file with name of your hosts or ip_address (like in
sample files). After that you are editing hosts.ini file and writing your destination host
ip or hostname (if you have configured own DNS server) in sections that you want install
on destination host. Now you have fully configured inventories. 
Before run playbook you have to create a vault file where you are storing yours keys that
will be used in installation process with full encryption. 

## Configuration vault:
1. Creating vault: ansible-vault create vaulted_vars.yml.
- You will be ask for a password which will be used for encryption of vault file.
2. Edit vault: ansible-vault edit vaulted_vars.yml
3. Store in vault passwords for yours hosts. Below you can find sample vault:
```
root_passwd: "Root Password in configuration device/VM"
srv-1_pass: "Name of new user"
srv-2_pass: "Name of new user"
srv-1_password_enc: "SHA512 - you can read how to generete hash below"
srv-2_password_enc: "SHA512 - you can read how to generete hash below"
server_srv-1_pub: "public key which will be upload to server for new user"
server_srv-2_pub: "public key which will be upload to server for new user"
openvpn_ca_password: "Password for encryption root CA"
openvpn_server_key_password: "Password for encryption server files - not used"
client_to_add_passwords:
 - "Password for 1 client"
 - "Password for 2 client"
 - "Password for 3 client" 
 - etc...
 ```

Now you can run the plybook and enjoy the automation.

## Run the playbook:

ansible-playbook -i invenotries/sample/hosts.ini server.yml -u USER --ask-vault-pass -e
ansible-python-interpreter=/usr/bin/python3


## How to generetes hash:
You can use command mkpasswd to generate hash, for example: 'mkpasswd -m
sha-512'. It returns sha-512 hash with random salts.
